# Publication Lineage Project

## Purpose

To create a standardized approach which easily and transparently shows the lineage of any publication, including the people who crafted, wrote, edited them and the organizations that support them. 

In addition, this project will also setup the processes and structures to grade publications based on lineage, and most importantly to continually improve that process so that it is a living, breathing activity.  


## Approach

### Key Tenets:

- Standardized model for capturing who worked on any publications, including but not limited to newspapers, magazines, scientific research, social media posts.  This model will include:
    - Any person who touches an article, including writers, editors and reviewers
    - Any Organization that supports the creation OR publication of an article, including syndication, reposting or any other way that it is distributed (public or private)
    - A way to classify publications, which will include an automated, ML model to scan and classify content, as well as a tagging system to allow creators of content to self-classify.  This two-pronged approach is critical, and will serve as a foundation for the third tenent of classification which is a comparison of the two.  

- Automation and streamlining of data capture and rating processes is key to the scalability of this model, so that it can be setup as a part of the canonical norm for understanding the sources of our data.

- Continuous improvement of the methodology is paramount.  No project is born whole, and the ability of this project to reflect not only new information gained as time goes on, but acceptance of the cultures in which it is used, is critical to its success.